package model_message;

import java.util.Date;
//import model.Statements;

/**
 *
 * @author xhz10atu
 */
public class Message {//implements Statements {

    private int messageId;
    private Date dateAndTime;
    private String subject;
    private int senderId;
    private int recieverId;
    private String message;
    private boolean read;
    private boolean sent;
    private boolean invitation;
    private boolean thread;

    public Message(int messageId, Date dateAndTime, String subject, int senderId,
            int recieverId, String message, boolean invitation, boolean thread) {

        this.messageId = messageId;
        this.dateAndTime = dateAndTime;
        this.subject = subject;
        this.senderId = senderId;
        this.recieverId = recieverId;
        this.message = message;
        this.read = false;
        this.sent = false;
        this.invitation = invitation;
        this.thread = thread;
    }

    public int getMessageId() {
        return messageId;
    }

    public Date getDateAndTime() {
        return dateAndTime;
    }

    public String getSubject() {
        return subject;
    }

    public int getSenderId() {
        return senderId;
    }

    public int getRecieverId() {
        return recieverId;
    }

    public String getMessage() {
        return message;
    }

    public void messageRead() {
        read = true;
    }

    public boolean isRead() {
        return read;
    }

    public void messageSent() {
        sent = true;
    }

    public boolean isSent() {
        return sent;
    }

    public boolean isInvitation() {
        return invitation;
    }

    public boolean isThread() {
        return thread;
    }

    public String getInsertSQLStatement() {

        return "INSERT INTO MESSAGE VALUES("
                + this.messageId
                + ", '" + this.dateAndTime
                + "', '" + this.subject
                + "', '" + this.message
                + "', " + this.sent
                + ", " + this.read
                + ", " + this.invitation
                + ", " + this.senderId
                + ", " + this.recieverId
                + ", " + this.thread + ");";
    }
    
    public String getUpdateSQLStatement() {
        return null;
    }
}