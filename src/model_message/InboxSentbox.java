package model_message;

import java.util.ArrayList;
/**
 *
 * @author xhz10atu
 */
public class InboxSentbox {

    private int inboxSentboxId;
    private ArrayList<Message> messageList;

    public InboxSentbox(int inboxSentboxId, ArrayList<Message> messageList) {
        
        this.inboxSentboxId = inboxSentboxId;
        this.messageList = messageList;
    }

    public int getInboxSentboxId() {
        return inboxSentboxId;
    }

    public ArrayList<Message> getMessageList() {
        return messageList;
    }

    public void removeMessage(Message message) {
        messageList.remove(message);
    }
}