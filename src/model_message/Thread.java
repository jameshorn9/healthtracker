package model_message;

import model_database.DatabaseAccess;
//import model.Statements;

/**
 *
 * @author xhz10atu
 */
public class Thread {//implements Statements {

    private int threadId;
    private String threadName;
    private int groupId;
    private int messageId;

    public Thread(int threadId, String threadName, int groupId, int messageId) {

        this.threadId = threadId;
        this.threadName = threadName;
        this.groupId = groupId;
        this.messageId = messageId;
    }

    public int getThreadId() {
        return threadId;
    }

    public String getThreadName() {
        return threadName;
    }

    public String getInsertSQLStatement() {

        return "INSERT INTO THREAD VALUES("
                + this.threadId
                + ", '" + this.threadName
                + "', " + this.groupId
                + ", " + this.messageId + ");";
    }

    public String getUpdateSQLStatement() {

        return "UPDATE THREAD SET "
                + "ThreadName = '" + this.threadName
                + "', GroupId = " + this.groupId
                + ", MessageId = " + this.messageId
                + " WHERE ThreadId = " + this.threadId + ";";
    }
}