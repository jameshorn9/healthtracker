package model_exercise;

/**
 *
 * @author qgk11uju
 */
public class Activity {
    
    private int activityId;
    private String name;
    private byte typeOfActivity;
    private int noOfSets;
    private int caloriesBurnt;

    public Activity(int activityId, String name, byte typeOfActivity, int noOfSets, int caloriesBurnt) {
        
        this.activityId     = activityId;
        this.name           = name;
        this.typeOfActivity = typeOfActivity;
        this.noOfSets       = noOfSets;
        this.caloriesBurnt  = caloriesBurnt;
    }

    public int getActivityId() {
        return activityId;
    }

    public String getName() {
        return name;
    }

    public byte getTypeOfActivity() {
        return typeOfActivity;
    }

    public int getNoOfSets() {
        return noOfSets;
    }

    public void setNoOfSets(int noOfSets) {
        this.noOfSets = noOfSets;
    }

    public int getCaloriesBurnt() {
        return caloriesBurnt;
    }

    public void setCaloriesBurnt(int userWeight, int totalDuration) {
        this.caloriesBurnt = (int) 0.75 * userWeight * totalDuration;
    }   
}