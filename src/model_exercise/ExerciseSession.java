package model_exercise;

import java.util.ArrayList;

/**
 *
 * @author qgk11uju
 */
public class ExerciseSession {
    
    private int exerciseSessionId;
    private String exerciseSessionName;
    private float totalDistance;
    private float totalDuration;
    private float totalCaloriesBurnt;
    private ArrayList<Integer> activityId;

    public ExerciseSession(int exerciseSessionId, String exerciseSessionName, float totalDistance, float totalDuration, float totalCaloriesBurnt, ArrayList<Integer> activityId) {
        
        this.exerciseSessionId   = exerciseSessionId;
        this.exerciseSessionName = exerciseSessionName;
        this.totalDistance       = totalDistance;
        this.totalDuration       = totalDuration;
        this.totalCaloriesBurnt  = totalCaloriesBurnt;
        this.activityId          = activityId;
    }

    public int getExerciseSessionId() {
        return exerciseSessionId;
    }

    public String getExerciseSessionName() {
        return exerciseSessionName;
    }

    public void setExerciseSessionName(String exerciseSessionName) {
        this.exerciseSessionName = exerciseSessionName;
    }

    public float getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(float[] totalDistance) {
        
        for (int i = 0; i < totalDistance.length; i++) {
            this.totalDistance += totalDistance[i];
        }
    }

    public float getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(float[] totalDuration) {
        
        for (int i = 0; i < totalDuration.length; i++) {
            this.totalDuration += totalDuration[i];
        }
    }

    public float getTotalCaloriesBurnt() {
        return totalCaloriesBurnt;
    }

    public void setTotalCaloriesBurnt(float[] totalCaloriesBurnt) {
        
        for (int i = 0; i < totalCaloriesBurnt.length; i++) {
            this.totalCaloriesBurnt = totalCaloriesBurnt[i];
        }
    }

    public ArrayList<Integer> getActivityId() {
        return activityId;
    }

    public void setActivityId(ArrayList<Integer> activityId) {
        this.activityId = activityId;
    }   
}
