package test;

import static org.junit.Assert.*;
import junit.framework.*;
import model_user.User;
import org.junit.Test;
import org.junit.Before;

public class UserTest {
	
	User user;
	
	@Before
	   public void setup() {
	      User user = new User(1,"Bob","Smith","bobby64","vol53k0m","bob.bob@bob.com");
	   }


		@Test
		public void testGetUserId() {	
			
		assertTrue("Should not be a negative number",user.getUserId() >=0);
	
	}
	


	@Test
	public void testGetInsertSQLStatement() {
		assertEquals("SQL statement is invalid!","INSERT INTO USERS VALUES('Bob','Smith','bobby64','vol53k0m','bob.bob@bob.com')");
		
	}

	@Test
	public void testGetUpdateSQLStatement() {
		assertEquals("SQL statement is invalid!","UPDATE USERS SET FirstName = 'Bob', LastName = 'Smith', Username = 'bobby64',Password" +
				" = 'vol53k0m', Email = 'bob.bob@bob.com', Gender = 'Male', AddressLine1 = 'Something', AddressLine2 = 'Else', " +
				"City = 'Tomorrowland', County = 'Generic', PostCode = 'NR47TJ', PhoneNumber = '12345 123 456', MobileNumber = '07531 123 123'" +
				", Height = '172', Weight = '70', BodyMassIndex = '22' WHERE UserID = 1");
			
}
		
	}


