package test;
import java.sql.*;
import model_database.DatabaseAccess;
import static org.junit.Assert.*;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.Before;
import model_user.User;


import static org.junit.Assert.*;

import model_database.DatabaseAccess;

import org.junit.Test;

public class DatabaseAccessTest {
	
	User user;
	Connection thisConnection;
	
	@Before
	public void setup(){
		try {
			User user = new User(1,"Bob","Smith","bobby64","vol53k0m","bob.bob@bob.com");
			Connection thisConnection = DatabaseAccess.getConnection();
			
		}
		catch (Exception e){
			System.out.println("An error occured: " + e);
		}
		
	}
	

	@Test
	public void testGetConnection() {
		try {
		
		assertEquals(true,DatabaseAccess.getConnection().isValid(0));
		}
		catch (Exception e){
			System.out.println("Blah blah " + e);
			
		}
		
		
		
	}

	@Test
	public void testRunUpdateQuery() {
		
		try {
		
		
		String sql = "DERP";
		assertEquals(false, DatabaseAccess.runUpdateQuery(sql, thisConnection));
		}
		catch (Exception e){
			System.out.println("Connection error.");
		}
		
	}

	@Test
	public void testGetQueryResults() {
		
		try {
			
			String sql = "SELECT * FROM USER";			
			assertEquals(DatabaseAccess.getQueryResults(sql, thisConnection)!=null,DatabaseAccess.getQueryResults(sql, thisConnection));
		}
		catch (Exception e){
			System.out.println(e);
		}
	}
	
	@Test
	public void testCreateUniqueID(){
		assertEquals("Should be +1 to the last existing ID",2);
		
	}
	
	

}
