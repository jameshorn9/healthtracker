package model_database;

import java.sql.*;
import javax.servlet.ServletException;

public class DatabaseAccess {

    // default constructor
    private DatabaseAccess() {}

    private static ResultSet set;
    
    // connect to database
    public static Connection getConnection() throws ServletException {

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new ServletException(String.format("Error: Cannot find JDBC driver..."), e);
        }

        try {
            // connect to database
            Connection connection = DriverManager.getConnection("jdbc:postgresql:postgres", "User", "");
            // return connection
            return (connection);
        } catch (SQLException e) {
            // throw error message if connection problem occurs
            throw new ServletException(String.format("Error: Database connection failed..."), e);
        }
    }

    // run query on database
    public static boolean runUpdateQuery(String sql, Connection thisConnection)
            throws ServletException {

        try {
            Statement statement = thisConnection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            statement.executeUpdate(sql);
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    public static ResultSet getQueryResults(String sql, Connection thisConnection) {

        try {
            Statement thisStatement = thisConnection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            set = thisStatement.executeQuery(sql);
        } catch (SQLException e) {
            System.out.println("Problem getting results from query!");
        }
        return set;
    }
    
        /*
     * This function is used for creating a unique number in the database for each table
     * that uses a primary key. It first of all calls the countRowsInTable function
     * and stores this value into the numOfIDs variable and then increments this
     * number by 1 and returns this value, to where it was called from.
     */
    public static int createUniqueID(String tableField, String tableName) {

        int numOfIDs = 0;

        try {

            numOfIDs = countRowsInTable(tableField, tableName);

            numOfIDs++;

        } catch (Exception e) {
            System.out.println("SHIT!");
        }

        return numOfIDs;
    }

    /*
     * This function gathers all data from the database from a specfic table name,
     * and where the tablefield in this case the idName is equal to the idvalue. Which
     * is set in the arguments and returns the this data as resultset to where it 
     * was called from.
     */
    public static ResultSet getDatabaseData(String tableName, String idName, int idValue) {

        try {

            String selectSQL = "SELECT * FROM " + tableName + " WHERE " + 
                    idName + " = " + idValue + ";";

            set = getQueryResults(selectSQL, getConnection());

        } catch (Exception e) {
            System.out.println("OH SHIT!");
        }

        return set;
    }

    /*
     * This function counts the number of rows which has the tablefield 
     * and tablename specified in the arguments using a built-in SQL
     * function called COUNT. Finally it returns the value to where it was called.
     */
    public static int countRowsInTable(String tableField, String tableName) {

        int numOfRows = 0;

        try {

            String countSQL = "SELECT COUNT(" + tableField + ") FROM " 
                    + tableName + ";";

            set = getQueryResults(countSQL, getConnection());

            while (set.next()) {
                numOfRows = set.getInt("count");
            }
        } catch (Exception e) {
            System.out.println("shit!");
        }

        return numOfRows;
    }
}