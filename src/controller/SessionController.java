package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model_database.DatabaseAccess;
import model_diet.Diet;
import model_diet.FoodAndDrink;
import model_group.Group;
import model_user.User;

/**
 *
 * @author xhz10atu
 */
@WebServlet(name = "SessionController", urlPatterns = {"/SessionController"})
public class SessionController extends HttpServlet {

    private static PrintWriter out;

    protected static void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        out = response.getWriter();

        try {

            Integer userId;

            //Get the user id from url link in email
            if (request.getParameter("account") != null) {

                userId = new Integer(request.getParameter("account").substring(0, 1));

            } else {

                userId = (Integer) request.getSession().getAttribute("userId");
            }

            gatherUserData(request, response, userId);

            gatherGroupData(request, response, userId);

            request.getSession().setAttribute("loggedIn", true);

            if (request.getParameter("account") != null) {
                request.getRequestDispatcher("Profile.jsp").
                        forward(request, response);
            }

        } catch (Exception e) {
            out.println(e);
        }
    }

    private static void gatherUserData(HttpServletRequest request,
            HttpServletResponse response, int userId)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        out = response.getWriter();

        try {

            ResultSet uSet = DatabaseAccess.getDatabaseData("USERS", "UserId",
                    userId);

            while (uSet.next()) {

                User user = new User(
                        userId,
                        uSet.getString("FirstName"),
                        uSet.getString("LastName"),
                        uSet.getString("Username"),
                        uSet.getString("Password"),
                        uSet.getString("Email"),
                        uSet.getBoolean("Gender"),
                        uSet.getString("AddressLine1"),
                        uSet.getString("Addressline2"),
                        uSet.getString("City"),
                        uSet.getString("County"),
                        uSet.getString("PostCode"),
                        uSet.getString("PhoneNumber"),
                        uSet.getString("MobileNumber"),
                        uSet.getFloat("Height"),
                        uSet.getFloat("Weight"),
                        uSet.getFloat("BodyMassIndex"));

                if (!uSet.getBoolean("AccountActivated")) {

                    String sql = String.format("UPDATE USERS SET "
                            + "AccountActivated = %b", true);

                    DatabaseAccess.runUpdateQuery(sql,
                            DatabaseAccess.getConnection());
                }

                request.getSession().setAttribute("user", user);
            }

        } catch (Exception e) {
            out.println(e);
        }
    }

    private static void gatherGroupData(HttpServletRequest request,
            HttpServletResponse response, int currentUserId)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        out = response.getWriter();

        try {

            ResultSet memberOrAdminIdsSet = DatabaseAccess.getDatabaseData("MEMBERORADMINOFGROUP", "UserId", currentUserId);

            ArrayList<Integer> groupIdList = new ArrayList<Integer>();

            ArrayList<Integer> groupAdminIdList =
                    new ArrayList<Integer>();

            ArrayList<Integer> groupMemberIdList =
                    new ArrayList<Integer>();

            while (memberOrAdminIdsSet.next()) {
                groupIdList.add(memberOrAdminIdsSet.getInt("GroupId"));
            }

            if (!groupIdList.isEmpty()) {

                ArrayList<Group> groupList = new ArrayList<Group>();

                for (Integer gId : groupIdList) {

                    memberOrAdminIdsSet = DatabaseAccess.getDatabaseData("MEMBERORADMINOFGROUP", "GroupId", gId);

                    while (memberOrAdminIdsSet.next()) {

                        int userId = memberOrAdminIdsSet.getInt("UserId");

                        /*
                         * If the the user belonging to the group is an
                         * member.
                         */
                        if (memberOrAdminIdsSet.
                                getBoolean("MemberOrAdmin")) {
                            groupMemberIdList.add(userId);
                        } else { //If it is an admin                   
                            groupAdminIdList.add(userId);
                        }
                    }

                    ResultSet groupSet = DatabaseAccess.getDatabaseData("GROUPS", "GroupId", gId);

                    groupSet.next();

                    groupList.add(new Group(
                            gId,
                            groupSet.getString("GroupName"),
                            groupSet.getString("Description"),
                            groupAdminIdList,
                            groupMemberIdList));
                }

                request.getSession().setAttribute("groupList", groupList);
            }

        } catch (Exception e) {
            out.println(e);
        }
    }

    private static void gatherDietData(HttpServletRequest request,
            HttpServletResponse response, int currentUserId)
            throws ServletException, IOException {

        try {

            response.setContentType("text/html;charset=UTF-8");
            out = response.getWriter();

            ResultSet dietFadSet = DatabaseAccess.getDatabaseData
                    ("DIET_FAD", "UserId", currentUserId);

            ArrayList<Integer> dietIdList = new ArrayList<Integer>();

            ArrayList<FoodAndDrink> fAndDrinkList = new ArrayList<FoodAndDrink>();

            ArrayList<Diet> dietList = new ArrayList<Diet>();

            while (dietFadSet.next()) {

                dietIdList.add(dietFadSet.getInt("DietId"));
            }

            if (!dietIdList.isEmpty()) {

                for (Integer dietId : dietIdList) {

                    dietFadSet = DatabaseAccess.getDatabaseData
                            ("DIET_FAD", "DietId", dietId);

                    while (dietFadSet.next()) {

                        fAndDrinkList.add(new FoodAndDrink(
                                dietFadSet.getInt("FoodAndDrinkId"),
                                dietFadSet.getString("FoodAndDrinkName"),
                                dietFadSet.getBoolean("FoodAndDrink"),
                                dietFadSet.getInt("calories")));
                    }

                    ResultSet dietSet = DatabaseAccess.getDatabaseData
                            ("DIET", "DietId", dietId);

                    while (dietSet.next()) {

                        dietList.add(new Diet(
                                dietId,
                                dietSet.getString("DietName"),
                                fAndDrinkList));
                    }
                }
                
                request.getSession().setAttribute("dietList", dietList);
            }

        } catch (Exception e) {

            out.println(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}