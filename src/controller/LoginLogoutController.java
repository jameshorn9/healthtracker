package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model_database.DatabaseAccess;

/**
 *
 * @author xhz10atu
 */
@WebServlet(name = "LoginLogoutController", urlPatterns = {"/LoginLogoutController"})
public class LoginLogoutController extends HttpServlet {

    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {

            //If the user wants to login
            if (request.getParameter("action").equals("login")) {
                
                //Can't log in till activated account
                
                Boolean loggedIn =
                        (Boolean) request.getSession().getAttribute("loggedIn");

                if (loggedIn != Boolean.TRUE) {

                    String username = request.getParameter("username"),
                           password = request.getParameter("password");

                    String sql = String.format("SELECT * FROM USERS WHERE "
                            + "Username = '%s' AND Password = '%s';",
                            username, password);

                    ResultSet resultSet = DatabaseAccess.getQueryResults(sql,
                            DatabaseAccess.getConnection());

                    String dbUser = "", dbPass = "";
                    int userId = 0;
                    boolean accountActivated = false;

                    while (resultSet.next()) {

                        userId = resultSet.getInt("UserId");
                        dbUser = resultSet.getString("Username");
                        dbPass = resultSet.getString("Password");
                        accountActivated = 
                                resultSet.getBoolean("AccountActivated");
                        
                        if(resultSet.wasNull()){
                            accountActivated = false;
                        }
                    }
            
                    if (username.equalsIgnoreCase(dbUser)
                            && password.equalsIgnoreCase(dbPass) &&
                            accountActivated) {

                        request.getSession().setAttribute("userId", userId);

                        loggedIn = Boolean.TRUE;

                    } else if(!accountActivated) {
                        request.getSession().setAttribute("accountActivated", 
                                false);
                        response.sendRedirect("Login.jsp");
                    }
                    else{
                        out.println("SHIT!");
                    }
                }

                if (loggedIn == Boolean.TRUE) {

                    SessionController.processRequest(request, response);
                    request.getRequestDispatcher("Profile.jsp").forward(request,
                            response);
                }
            } else { //If users wants to logout
                
                request.getSession().invalidate();
                response.sendRedirect("Home.jsp");
            }
            
        } catch (Exception e) {
            out.println(e);
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}