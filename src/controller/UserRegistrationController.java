package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model_database.DatabaseAccess;
import model_user.User;

/**
 *
 * @author qgk11uju
 */
@WebServlet(name = "UserRegistrationController", urlPatterns = {"/UserRegistrationController"})
public class UserRegistrationController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {
 
            User user = new User(
                    DatabaseAccess.createUniqueID("UserId", "USERS"), 
                    request.getParameter("firstName"),
                    request.getParameter("lastName"),
                    request.getParameter("username"),
                    request.getParameter("password"),
                    request.getParameter("email"));
            		
            
            DatabaseAccess.runUpdateQuery(user.getInsertSQLStatement(),
                    DatabaseAccess.getConnection());
            
            request.getSession().setAttribute("user", user);

            request.getRequestDispatcher(
                    "SendEmailConfirmationController").forward(
                    request, response);
            

        } catch (Exception e) {
            out.println(e);
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}