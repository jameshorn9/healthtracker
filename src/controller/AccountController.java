package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model_database.DatabaseAccess;
import model_user.User;

/**
 *
 * @author xhz10atu
 */
@WebServlet(name = "AccountController", urlPatterns = {"/AccountController"})
public class AccountController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {

            boolean gender = true;

            if (request.getParameter("gender").equals("female")) {
                gender = false;
            }

            final double inchesToMetresRatio = 0.0254;
            final double poundToKgRatio = 0.453592; 
            
            double height = 0, weight = 0;
            
//            String heightStr = request.getParameter("height"),
//                   weightStr = request.getParameter("weight");
//            
//            String [] feetAndInchesHeightStr;
//            
//            if(heightStr.contains(".")){
//                height = Double.parseDouble(request.getParameter("height"));             
//            }
//            else {
                
//                feetAndInchesHeightStr = heightStr.split("'");
//                
//                double feetInMetres = 
//                        (Double.parseDouble(feetAndInchesHeightStr[0]) * 12 *
//                        inchesToMetresRatio);
//                
//                double inchesToMetres = 
//                        (Double.parseDouble(feetAndInchesHeightStr[1]) *
//                        inchesToMetresRatio);
                        
                
                       
            //}       
//                    double bMI = 
//                    (Math.round((weight / (height * height)) * 100.0f) / 100.0f);

            User user = (User) request.getSession().getAttribute("user");

            user.setGender(gender);
            user.setAddressLine1(request.getParameter("addressLine1"));
            user.setAddressLine2(request.getParameter("addressLine2"));
            user.setCity(request.getParameter("city"));
            user.setCounty(request.getParameter("county"));
            user.setPostcode(request.getParameter("postcode"));
            user.setPhoneNumber(request.getParameter("phoneNumber"));
            user.setMobileNumber(request.getParameter("mobileNumber"));
            user.setHeight(0);
            user.setWeight(0);
            user.setBMI(0);

            DatabaseAccess.runUpdateQuery(user.getUpdateSQLStatement(),
                    DatabaseAccess.getConnection());

            request.getSession().setAttribute("user", user);
                    
            request.getSession().setAttribute("fullSignUp", true);
            
            request.getRequestDispatcher("Profile.jsp").
                    forward(request, response);

        } catch (Exception e) {
            out.println(e);
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}