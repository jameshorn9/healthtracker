package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model_user.User;

/**
 *
 * @author xhz10atu
 */
@WebServlet(name = "SendEmailConfirmationController", urlPatterns = {"/SendEmailConfirmationController"})
public class SendEmailConfirmationController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {

            User user = (User) request.getSession().getAttribute("user");

            // Recipient's email ID needs to be mentioned.
            final String to = user.getEmail();

            // Sender's email ID needs to be mentioned
            final String from = "yolohealthtracker@gmail.com";

            // Get system properties
            Properties props = System.getProperties();

            // Setup mail server
            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.auth", "true");
            props.put("mail.debug", "false");
            props.put("mail.smtp.ssl.enable", "true");

            // Get the default Session object.
            Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {

                     return new PasswordAuthentication(from, "hornewillis");
                }
                    
            });
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(to));

            // Set Subject: header field
            message.setSubject("Confirm your account for YoloHealthTracker!");
            
            final String code = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            
            String randomlyGeneratedCode = "";
            
            randomlyGeneratedCode += user.getUserId();
            
            for(int i = 0; i < 8; i++){
                
                randomlyGeneratedCode += 
                        code.charAt(0 + (int)(Math.random()* code.length() - 1));
            }
            
            // Send the actual HTML message, as big as you like
            message.setContent("<title>Click the link below to activate "
                    + "your account.</title><br />"
                    + "<a href=\"http://localhost:8084/HealthTracker/"
                    + "SessionController?account="+ randomlyGeneratedCode +
                    "t7y57k5hl6j3po\">Click to activate your account!</a>", 
                    "text/html");

            // Send message
            Transport.send(message);

            request.getRequestDispatcher(
                    "AccountConfirmation.jsp").forward(
                    request, response);

        } catch (Exception e) {
            out.println(e);
        }
    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}