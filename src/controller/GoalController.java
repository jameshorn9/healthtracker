package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model_database.DatabaseAccess;
import model_goal.Goal;

/**
 *
 * @author qgk11uju
 */

@WebServlet(name = "GoalController", urlPatterns = {"/GoalController"})
public class GoalController extends HttpServlet {
    
    private static PrintWriter out;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        out = response.getWriter();
        
        try {
            
            String typeOfGoal = request.getParameter("goal");
            
            if (typeOfGoal.equalsIgnoreCase("weight")) {
                createWeightGoal(request, response);
            }
            
            
        } catch (Exception e) {
            out.println(e);
        }
    
    }
    
    private static void createWeightGoal(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        out = response.getWriter();
        
        try {
            int typeOfGoal = Integer.parseInt(request.getParameter("typeOfGoal"));
            
            Goal goal = new Goal(
                    DatabaseAccess.createUniqueID("GoalId", "GOAL"),
                    Integer.parseInt(request.getParameter("userId")),
                    request.getParameter("goalName"),
                    request.getParameter("goalDescription"),
                    typeOfGoal);

            switch(typeOfGoal) {
                case 0 : goal.setTargetWeight(Float.parseFloat(request.getParameter("targetWeight")));
                break;
                case 1 : goal.setTargetBMI(Float.parseFloat(request.getParameter("targetBMI")));
                break;
                case 2 : goal.setTargetDistance(Float.parseFloat(request.getParameter("targetDistance")), 
                    Float.parseFloat(request.getParameter("targetDuration")));
                break;
                case 3 : goal.setTargetRepsAndWeight(Integer.parseInt(request.getParameter("targetReps")), 
                    Float.parseFloat(request.getParameter("targetWeightLifted")));
                break;
                case 4 : goal.setTargetCaloriesBurnt(Integer.parseInt(request.getParameter("targetCaloriesBurnt")));
                break;
                case 5 : goal.setTargetCaloriesConsumed(Integer.parseInt(request.getParameter("targetCaloriesConsumed")));
                break;
            }
           
            DatabaseAccess.runUpdateQuery(goal.getInsertSQLStatement(),
                    DatabaseAccess.getConnection());
            
            request.getSession().setAttribute("goal", goal);
            
        } catch (Exception e) {
            out.println(e);
        }
    }
}
