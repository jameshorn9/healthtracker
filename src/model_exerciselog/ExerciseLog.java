package model_exerciselog;

import java.util.ArrayList;

/**
 *
 * @author qgk11uju
 */
public class ExerciseLog {
    
    private int exerciseLogId;
    private int userId;
    private ArrayList<ActivitySet> activitySet;

    public ExerciseLog(int exerciseLogId, int userId, ArrayList<ActivitySet> activitySet) {
        
        this.exerciseLogId = exerciseLogId;
        this.userId        = userId;
        this.activitySet   = activitySet;
    }

    public int getExerciseLogId() {
        return exerciseLogId;
    }

    public int getUserId() {
        return userId;
    }

    public ArrayList<ActivitySet> getActivitySet() {
        return activitySet;
    }

    public void setActivitySet(ArrayList<ActivitySet> activitySet) {
        this.activitySet = activitySet;
    }
}
