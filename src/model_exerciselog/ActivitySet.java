package model_exerciselog;
import java.util.Date;

/**
 *
 * @author qgk11uju
 */
public class ActivitySet {
    
    private int activitySetId;
    private Date dateAndTime;
    private float distance;
    private float duration;
    private int reps;
    private float weight;
    private int activityId;

    public ActivitySet(int activitySetId, Date dateAndTime, float distance, float duration, int reps, float weight, int activityId) {
        
        this.activitySetId = activitySetId;
        this.dateAndTime   = dateAndTime;
        this.distance      = distance;
        this.duration      = duration;
        this.reps          = reps;
        this.weight        = weight;
        this.activityId    = activityId;
    }

    public int getActivitySetId() {
        return activitySetId;
    }

    public Date getDateAndTime() {
        return dateAndTime;
    }

    public float getDistance() {
        return distance;
    }

    public float getDuration() {
        return duration;
    }

    public int getReps() {
        return reps;
    }

    public float getWeight() {
        return weight;
    }

    public int getActivityId() {
        return activityId;
    }
}