package model_group;

import java.util.ArrayList;
import model_user.User;

public class Group {//implements Statements{
    
    private int groupId;
    private String groupName;
    private String description;
    private ArrayList<Integer> admin = new ArrayList<Integer>();
    private ArrayList<Integer> member = new ArrayList<Integer>();

    public Group() {}

    public Group(int groupId, String nameGroup, String dText, 
            ArrayList<Integer> admin, ArrayList<Integer> member) {
        
        this.groupId = groupId;
        this.groupName = nameGroup;
        this.description = dText;
        this.admin = admin;
        this.member = member;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setGroupDescription(String description) {
        this.description = description;
    }

    public void setAdmin(ArrayList<Integer> admin) {
        this.admin = admin;
    }

    public void setMember(ArrayList<Integer> member) {
        this.member = member;
    }

    public int getGroupId() {
        return groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public String getGroupDescription() {
        return description;
    }

    public ArrayList<Integer> getAdmin() {
        return admin;
    }

    public ArrayList<Integer> getMember() {
        return member;
    }

    public void addMember(User user) {

        member.add(user.getUserId());

        //Take userID from user
        //Insert userID to memberArray

    }

    public void promoteToAdmin(User user) {
        
        if (member.contains(user.getUserId())) {
            member.remove(user.getUserId());
            admin.add(user.getUserId());
        } else {
            admin.add(user.getUserId());
        }
        //Take userID
        //Remove from memberArray
        //Add to adminArray
    }


    public String getInsertSQLStatement() {
        
         return "INSERT INTO GROUPS VALUES("
                + this.groupId
                + ", '" + this.groupName
                + "', '" + this.description + "');";
    }
    
    public String getUpdateSQLStatement() {
        
        return "UPDATE GROUPS SET " 
             + "GroupName = '" + this.groupName
             + "', Description = '" + this.description                     
             + "' WHERE GroupId = " + this.groupId + ";";
    }
}