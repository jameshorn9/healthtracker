package model_diet;

import java.util.ArrayList;

/**
 *
 * @author xhz10atu
 */
public class Diet {

    private int dietId;
    private String dietName;
    private int totalCalories;
    private ArrayList<FoodAndDrink> foodAndDrinkList;

    public Diet(int dietId, String dietName, ArrayList<FoodAndDrink> foodAndDrinkList) {
        
        this.dietId = dietId;
        this.dietName = dietName;
        this.foodAndDrinkList = foodAndDrinkList;
        
        for(FoodAndDrink fD : foodAndDrinkList){
            this.totalCalories += fD.getCalorieCount();
        }
    }

    public void setTotalCalories() {

        for (FoodAndDrink fD : foodAndDrinkList) {
            totalCalories += fD.getCalorieCount();
        }
    }

    public int getDietId(){
        return dietId;
    }
    
    public String getDietName() {
        return dietName;
    }

    public int getTotalCalories() {
        return totalCalories;
    }

    public void addFoodDrink(FoodAndDrink fDItem) {
        foodAndDrinkList.add(fDItem);
    }

    public ArrayList<FoodAndDrink> getFoodDrink() {
        return foodAndDrinkList;
    }
}