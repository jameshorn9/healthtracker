package model_diet;

/**
 *
 * @author xhz10atu
 */
public class FoodAndDrink {

    private int foodDrinkId;
    private String foodDrinkName;
    private boolean foodOrDrink;
    private int calorieCount;

    public FoodAndDrink(int foodDrinkId, String foodDrinkName, 
            boolean foodOrDrink, int calorieCount) {
        
        this.foodDrinkId = foodDrinkId;
        this.foodDrinkName = foodDrinkName;
        this.foodOrDrink = foodOrDrink;
        this.calorieCount = calorieCount;
    }

    public int getFoodDrinkId(){
        return foodDrinkId;
    }
    
    public String getFoodDrinkName() {
        return foodDrinkName;
    }

    public boolean isFoodOrDrink() {
        return foodOrDrink;
    }

    public int getCalorieCount() {
        return calorieCount;
    }
}