/*
 * MILES, JAMES AND MATT
 * UPDATED BY MATT 27/03/13
 */

package model_goal;

import model_user.User;
import java.util.*;
import java.text.*;

public class Goal {

    private int goalId;
    private int userId;
    private String goalName;
    private String goalDescription;
    private int typeOfGoal;
    private float targetWeight;
    private float targetBMI;
    private int targetCaloriesConsumed;
    private float targetDistance;
    private float targetDuration;
    private int targetReps;
    private float targetWeightLifted;
    private int targetCaloriesBurnt;
    private Date goalEndDate;
    private boolean goalCompleted;

    public Goal() {}

    public Goal(int goalId, int userId, String goalName, String goalDescription, 
            int typeOfGoal) {

        this.goalId          = goalId;
        this.userId          = userId;
        this.goalName        = goalName;
        this.goalDescription = goalDescription;
        this.typeOfGoal      = typeOfGoal;
    }

    public String getGoalName() {
        return goalName;
    }

    public String getGoalDescription() {
        return goalDescription;
    }

    public int getTypeOfGoal() {
        return typeOfGoal;
    }

    public float getTargetWeight() {
        return targetWeight;
    }

    public float getTargetBMI() {
        return targetBMI;
    }

    public int getTargetCaloriesConsumed() {
        return targetCaloriesConsumed;
    }

    public float getTargetDistance() {
        return targetDistance;
    }

    public float getTargetDuration() {
        return targetDuration;
    }

    public int getTargetReps() {
        return targetReps;
    }

    public float getTargetWeightLifted() {
        return targetWeightLifted;
    }

    public int getTargetCaloriesBurnt() {
        return targetCaloriesBurnt;
    }
    
    public Date getGoalEndDate(){
        return goalEndDate;
    }
    
    
    
    public void setTargetWeight(float targetWeight) {
        this.targetWeight = targetWeight;
    }
    
    public void setTargetBMI(float targetBMI) {
        this.targetBMI = targetBMI;
    }
    
    public void setTargetDistance(float targetDistance, float targetDuration) {
        this.targetDistance = targetDistance;
        this.targetDuration = targetDuration;
    }
    
    public void setTargetRepsAndWeight(int targetReps, float targetWeightLifted) {
        this.targetReps         = targetReps;
        this.targetWeightLifted = targetWeightLifted;
    }
    
    public void setTargetCaloriesBurnt(int targetCaloriesBurnt) {
        this.targetCaloriesBurnt = targetCaloriesBurnt;
    }
    
    public void setTargetCaloriesConsumed(int targetCaloriesConsumed) {
        this.targetCaloriesConsumed = targetCaloriesConsumed;
    }
    
    public void setGoalEndDate(Date goalEndDate)
    {
        this.goalEndDate = goalEndDate;
    }
    
    public boolean checkWeight(User user) {

        if (targetWeight == user.getWeight()) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean goalCompleted()
    {
        if ()
    }

    public boolean checkBMI(User user) {

        if (targetBMI == user.getBMI()) {
            return true;
        } else {
            return false;
        }
    }
    
    public String getInsertSQLStatement() {

        return "INSERT INTO USERS VALUES("
                +          this.goalId
                + ", "   + this.userId
                + ", '"  + this.goalName
                + "', '" + this.goalDescription
                + "', "  + this.typeOfGoal
                + ", "   + this.targetWeight
                + ", "   + this.targetBMI
                + ", "   + this.targetCaloriesConsumed 
                + ", "   + this.targetDistance
                + ", "   + this.targetDuration
                + ", "   + this.targetReps
                + ", "   + this.targetWeightLifted
                + ", "   + this.targetCaloriesBurnt
                + ", "   + this.goalEndDate
                + ");";
    }
}