package model_user;

import java.util.ArrayList;
import model_diet.Diet;
import model_diet.FoodAndDrink;
import model_exercise.Activity;
import model_exercise.ExerciseSession;
import model_goal.Goal;
import model_message.InboxSentbox;

public class User {//implements Statements {

    protected int userId;
    protected String username;
    protected String password;
    protected String firstName;
    protected String lastName;
    protected String email;
    protected boolean gender;
    protected String addressLine1;
    protected String addressLine2;
    protected String city;
    protected String county;
    protected String postcode;
    protected String phoneNumber;
    protected String mobileNumber;
    protected float height;
    protected float weight;
    protected float bodyMassIndex;
    protected InboxSentbox inbox;
    protected ArrayList<Integer> adminOfGroup;
    protected ArrayList<Integer> memberOfGroup;
    protected ArrayList<ExerciseSession> exerciseSession;
    protected ArrayList<Activity> personalActivity;
    protected ArrayList<Diet> diet;
    protected ArrayList<FoodAndDrink> foodAndDrink;
    protected ArrayList<Goal> goals;

    public User() {}

    public User(int userId, String firstName, String lastName,
            String username, String password, String email, boolean gender, 
            String addressLine1, String addressLine2, String city, 
            String county, String postcode, String phoneNumber, 
            String mobileNumber, float height, float weight, 
            float bodyMassIndex) {

        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.email = email;
        this.gender = gender;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.county = county;
        this.postcode = postcode;
        this.phoneNumber = phoneNumber;
        this.mobileNumber = mobileNumber;
        this.height = height;
        this.weight = weight;
        this.bodyMassIndex = bodyMassIndex;
    }
    
    //Partial sign up constructor
    public User(int userId, String firstName, String lastName,
            String username, String password, String email) {

        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.email = email;
    }
    
    public final int getUserId() {
        return userId;
    }

    public final String getUsername() {
        return username;
    }

    public final String getPassword() {
        return password;
    }

    public final String getEmail() {
        return email;
    }

    public final String getFirstname() {
        return firstName;
    }

    public final String getLastname() {
        return lastName;
    }

    public final boolean isGender() {
        return gender;
    }

    public final String getAddressLine1() {
        return addressLine1;
    }

    public final String getAddressLine2() {
        return addressLine2;
    }
    
    public final String getCity(){
        return city;
    }
    
    public final String getCounty(){
        return county;
    }

    public final String getPostcode() {
        return postcode;
    }

    public final String getPhoneNumber() {
        return phoneNumber;
    }

    public final String getMobileNumber() {
        return mobileNumber;
    }

    public final float getHeight() {
        return height;
    }

    public final float getWeight() {
        return weight;
    }

    public final float getBMI() {
        return bodyMassIndex;
    }

    public final InboxSentbox getInbox() {
        return inbox;
    }

    public final ArrayList<Integer> getAdminOfGroup() {
        return adminOfGroup;
    }

    public final ArrayList<Integer> getMemberOfGroup() {
        return memberOfGroup;
    }

    public final ArrayList<ExerciseSession> getExerciseSession() {
        return exerciseSession;
    }

    public final ArrayList<Activity> getPersonalActivity() {
        return personalActivity;
    }

    public final ArrayList<Diet> getDiet() {
        return diet;
    }

    public final ArrayList<FoodAndDrink> getFoodAndDrink() {
        return foodAndDrink;
    }

    public final ArrayList<Goal> getGoals() {
        return goals;
    }

    public final void setUsername(String username) {
        this.username = username;
    }

    public final void setPassword(String password) {
        this.password = password;
    }

    public final void setEmail(String email) {
        this.email = email;
    }

    public final void setGender(boolean gender){
        this.gender = gender;
    }
    
    public final void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public final void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }
    
    public final void setCity(String city){
        this.city = city;
    }
    
    public final void setCounty(String county){
        this.county = county;
    }

    public final void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public final void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public final void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public final void setHeight(float height) {
        this.height = height;
    }

    public final void setWeight(float weight) {
        this.weight = weight;
    }

    public final void setBMI(float bodyMassIndex) {
        this.bodyMassIndex = bodyMassIndex;
    }

    public final void setInbox(InboxSentbox inbox) {
        this.inbox = inbox;
    }

    public final void setAdminOfGroup(ArrayList<Integer> adminOfGroup) {
        this.adminOfGroup = adminOfGroup;
    }

    public final void setMemberOfGroup(ArrayList<Integer> memberOfGroup) {
        this.memberOfGroup = memberOfGroup;
    }

    public final void setExerciseSession(ArrayList<ExerciseSession> exerciseSession) {
        this.exerciseSession = exerciseSession;
    }

    public final void setPersonalActivity(ArrayList<Activity> personalActivity) {
        this.personalActivity = personalActivity;
    }

    public final void setDiet(ArrayList<Diet> diet) {
        this.diet = diet;
    }

    public final void setFoodAndDrink(ArrayList<FoodAndDrink> foodAndDrink) {
        this.foodAndDrink = foodAndDrink;
    }

    public final void setGoals(ArrayList<Goal> goals) {
        this.goals = goals;
    }

    public String getInsertSQLStatement() {

        return "INSERT INTO USERS VALUES("
                + this.userId
                + ", '" + this.firstName
                + "', '" + this.lastName
                + "', '" + this.username
                + "', '" + this.password
                + "', '" + this.email
                + "', " + null
                + ",  '" 
                + "', '"
                + "', '"
                + "', '"
                + "', '"
                + "', '"
                + "', '"
                + "', " + 0
                + ", " + 0
                + ", " + 0 
                + ", " + false + ");";
    }

    public String getUpdateSQLStatement() {

        return "UPDATE USERS SET "
                + "   FirstName = '" + this.firstName
                + "', LastName = '" + this.lastName
                + "', Username = '" + this.username
                + "', Password = '" + this.password
                + "', Email = '" + this.email
                + "', Gender = " + this.gender
                + ",  AddressLine1 = '" + this.addressLine1
                + "', AddressLine2 = '" + this.addressLine2
                + "', City = '" + this.city
                + "', County = '" + this.county
                + "', PostCode = '" + this.postcode
                + "', PhoneNumber = '" + this.phoneNumber
                + "', MobileNumber = '" + this.mobileNumber
                + "', Height = " + this.height
                + ",  Weight = " + this.weight
                + ",  BodyMassIndex = " + this.bodyMassIndex
                + "   WHERE UserID = " + this.userId + ";";
    }
    
//    @Override
//    public String toString(){
//        
//        StringBuilder userDetails = new StringBuilder();            
//    }
}